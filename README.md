
# SauceDemo Katalon Automation

Sauce Demo Automation Testing using Katalon Studio with 100% BDD Testing


## Installation

Just clone this repo and open it with Katalon Studio. It's easy as that!

    
## 🚀 About Me
My name is Rifqi Fadhilah, I'm Quality Assurance Engineer hybrid (Manual and Automation). I have 4 years of experience in Quality Assurance and ever working in several big companies like PT. Pegadaian, Segari, Skul.id etc.

## 🔗 Links
[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://gitlab.com/automation-testing-portofolio) 

[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/rifqi-fadhilah/)


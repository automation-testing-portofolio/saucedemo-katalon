@login
Feature: Login to Sauce Demo

  @loginPositive
  Scenario Outline: Login to Sauce Demo
    Given User open Sauce Demo webpage
    When User input <username> and <password>
    Then User redirected to Products page

    Examples: 
      | username       | password     |
      | standard_user  | secret_sauce |
      
    @loginNegative
    Scenario Outline: Failed to Login
     Given User open Sauce Demo webpage
     When User input <username> and <password>
     Then Display login failed error
     
      Examples: 
      | username       | password     |
      | asmlkaml  | jaskmxiemndxi |
      
    @loginLocked
    Scenario Outline: Login with Locked User
    Given User open Sauce Demo webpage
    When User input <username> and <password>
    Then Display login failed error
    
    Examples:
    | username       | password     |
    | locked_out_user | secret_sauce |
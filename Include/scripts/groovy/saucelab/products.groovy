package saucelab
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.remote.server.handler.FindElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import com.kms.katalon.core.logging.KeywordLogger



class products {
	KeywordLogger logger = new KeywordLogger()
	List <String> products = Arrays.asList(
	"sauce-labs-backpack",
	"sauce-labs-bike-light",
	"sauce-labs-bolt-t-shirt"
	)
	@Given("User is on Products Page")
	def UserisOnProductPage() {
		WebUI.verifyElementPresent(new TestObject().addProperty("id",ConditionType.EQUALS,"inventory_container"), 2)
	}

	@When("User click add to cart button")
	def addItems() {
		for (String product : products) {
			TestObject btn_addToCart = new TestObject().addProperty("id", ConditionType.EQUALS,"add-to-cart-"+product)
			WebUI.click(btn_addToCart)
			logger.logInfo("Item "+product+" has been added to cart")
			WebUI.delay(2)
		}
	}

	@Then("Items is added to cart")
	def verifyItems() {
		//TODO Fix looping later
		WebUI.click(new TestObject().addProperty("id",ConditionType.EQUALS, "shopping_cart_container"))
		TestObject cartText =  new TestObject().addProperty("xpath",ConditionType.EQUALS,"//span[@class='title']")
		String txt_cart = WebUI.getText(cartText)
		logger.logInfo("On "+txt_cart+" page")

		for(String product : products) {
			TestObject cartItem = new TestObject().addProperty("xpath",ConditionType.EQUALS,"//div[@class='inventory_item_name'and text()='"+ product.replace("-"," ")+"']")
			WebUI.verifyElementPresent(cartItem, 10)
			logger.logInfo(product+" is on Shopping Cart")
		}
	}
}
package saucelab
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.logging.KeywordLogger

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class login {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	KeywordLogger logger = new KeywordLogger()
	@Given("User open Sauce Demo webpage")
	def open_website() {
		WebUI.openBrowser('')
		WebUI.setViewPortSize(1280, 720)
		WebUI.navigateToUrl(GlobalVariable.baseURL)
		WebUI.waitForElementPresent(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//div[@class='login_wrapper']"), 0)
	}

	@When("User input (.*) and (.*)")
	def inputLogin(String username, String password) {
		TestObject formUsername = new TestObject().addProperty("id", ConditionType.EQUALS, "user-name")
		TestObject formPassword = new TestObject().addProperty("id", ConditionType.EQUALS, "password")
		TestObject btnLogin = new TestObject().addProperty("id", ConditionType.EQUALS, "login-button")

		WebUI.setText(formUsername, username)
		String txtUsername = WebUI.getAttribute(formUsername, 'value')
		logger.logInfo("Username: "+txtUsername)

		WebUI.setText(formPassword, password)
		String txtPassword = WebUI.getAttribute(formPassword, 'value')
		logger.logInfo("Password: "+txtPassword)

		WebUI.click(btnLogin)
		WebUI.delay(3)
	}

	@Then("User redirected to Products page")
	def loginPass() {
		WebUI.delay(3)
		WebUI.verifyElementPresent(new TestObject().addProperty("id", ConditionType.EQUALS, "inventory_container"), 0)
	}

	@Then("Display login failed error")
	def displayLoginError() {
		TestObject divError = new TestObject().addProperty("xpath", ConditionType.EQUALS, "//h3[@data-test='error']")
		String txtError = WebUI.getText(divError)

		WebUI.verifyElementPresent(divError, 2)
		logger.logInfo("Error: "+txtError)
		WebUI.closeBrowser()
	}
}
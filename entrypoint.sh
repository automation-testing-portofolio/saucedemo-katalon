#!/bin/sh
# entrypoint.sh

# Start virtual frame buffer
Xvfb :99 -screen 0 1920x1080x24 &

# Run Katalon Studio
$KATALON_DIR/katalon "$@"
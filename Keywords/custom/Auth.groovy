package custom
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


public class Auth {
	public static Map<String, String> getUserId(String role){
		TestData userCredentials = findTestData('Data Files/User')
		String username, password
		
		for (def i=1; i <= userCredentials.getRowNumbers(); i++) {
			if(userCredentials.getValue('role', i)==role) {
				username = userCredentials.getValue('username', i)
				password = userCredentials.getValue('password', i)
				break
			}
		}
		return [username: username, password: password]
	}
	
	public static void LoginPage(String role) {
		Map<String, String> credentials = getUserId(role)
		String username = credentials.username
		String password = credentials.password
		
		WebUI.maximizeWindow()
		
		TestObject formUsername = new TestObject().addProperty('id', ConditionType.EQUALS, 'user-name')
		TestObject formPassword = new TestObject().addProperty('id', ConditionType.EQUALS, 'password')
		TestObject btn_login = new TestObject().addProperty('id', ConditionType.EQUALS, 'login-button')
		
		WebUI.setText(formUsername, username)
		WebUI.setText(formPassword, password)
		WebUI.click(btn_login)
		WebUI.delay(3)
		
		WebUI.verifyElementClickable(new TestObject().addProperty('class', ConditionType.EQUALS, 'product_sort_container'), 3)
		WebUI.verifyTextPresent(, false)
		
		
	}
}
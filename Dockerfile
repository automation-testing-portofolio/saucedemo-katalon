# Base image
FROM ubuntu:22.04

# Set environment variables
ENV KATALON_VERSION=9.4.0
ENV KATALON_DIR=/opt/katalonstudio
ENV DISPLAY=:99

# Install dependencies
RUN apt-get update && apt-get install -y \
    openjdk-11-jdk \
    wget \
    unzip \
    xvfb \
    libnss3 \
    libxss1 \
    alsa-utils \
    fonts-liberation \
    libappindicator3-1 \
    xdg-utils \
    chromium-browser \
    libdbus-glib-1-2 \
    && rm -rf /var/lib/apt/lists/* \
    && useradd -m katalon \
    && mkdir -p ${KATALON_DIR}

# Download and install libgconf-2-4 from a trusted source
RUN wget http://archive.ubuntu.com/ubuntu/pool/universe/g/gconf/gconf2-common_3.2.6-6ubuntu1_all.deb \
    && dpkg -i gconf2-common_3.2.6-6ubuntu1_all.deb \
    && wget http://archive.ubuntu.com/ubuntu/pool/universe/g/gconf/libgconf-2-4_3.2.6-6ubuntu1_amd64.deb \
    && dpkg -i libgconf-2-4_3.2.6-6ubuntu1_amd64.deb \
    && apt-get install -f \
    && rm gconf2-common_3.2.6-6ubuntu1_all.deb \
    && rm libgconf-2-4_3.2.6-6ubuntu1_amd64.deb

# Download and install Katalon Studio

RUN wget https://download.katalon.com/${KATALON_VERSION}/Katalon_Studio_Engine_Linux_${KATALON_VERSION}.tar.gz -O /tmp/katalonstudio.tar.gz \
# RUN wget https://github.com/katalon-studio/katalon-studio/releases/download/${KATALON_VERSION}/Katalon_Studio_Engine_Linux_64-${KATALON_VERSION}.tar.gz -O /tmp/katalonstudio.tar.gz \
    && tar -xzf /tmp/katalonstudio.tar.gz -C ${KATALON_DIR} --strip-components=1 \
    && rm /tmp/katalonstudio.tar.gz

# Set up entrypoint
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

# Set working directory
WORKDIR ${KATALON_DIR}

# Define default command
CMD ["katalon-execute.sh"]

# Add volume to hold Katalon Studio project
VOLUME ["/katalon/project"]

# Expose port for Katalon Studio (if necessary)
EXPOSE 50000
